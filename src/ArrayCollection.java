/*
В этом практическом задании Вам необходимо реализовать интерфейс Collection на базе массива. Внимательно ознакомьтесь с документацией по интерфейсу Collection.
Реализовать необходимо только не default методы, а именно:
boolean	add(E e) - если размер массива уже равен size, то увеличиваем массив вдвое. Тут возвращаем true.
boolean	addAll(Collection<? extends E> c)
void	clear()
boolean	contains(Object o)
boolean	containsAll(Collection<?> c)  -  сравнивает элементы на входе, если есть все передаваемые элементы совпадают с массивом
boolean	equals(Object o)
int	hashCode()
boolean	isEmpty()
Iterator	iterator()
boolean	remove(Object o) - бежим по объектам массива до size. Удаляем совпадаюший элемент. через копирование всеъ элементов, кроме найденного
boolean	removeAll(Collection<?> c)
boolean	retainAll(Collection<?> c)
int	size()

Object[] toArray() - Преобразование в Array, преобразуем нашу коллекцию в Array. создаем новый массив размером size. Копируем в новый массив элементы старого. Возвращаем новый массив.

T[]	toArray(T[] a) - в случае, если переданный массив А имеет размер больший или равный размеру текущей колекции, то метод должен скопировать
все элементы текущей колекции в массив А. И вернуть массив А. Если же А меньше размера коллекции, то метод должен создать новый массив,
который имеет тот же тип что и массив А, но при этом имеет длинну равную длинне колекции. После, метод должен скопировать все элементы колекции
в новый массив. И вернуть его. Корректность Вашей реализации будет проверена Unit тестами, которые Вы можете найти в классе ArrayCollectionTest.
Имя класса, в котором Вам необходимо написать реализацию: ArrayCollection.
Можно использовать метод toArray - написанный ранее.
Вызовем метод toArray и приведем к типу T.
*/

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

import static com.sun.javafx.fxml.expression.Expression.not;

public class ArrayCollection<T> implements Collection<T>{

    private T[] m = (T[])new Object[1];

    private int size;

    @Override
    public int size() {
        // BEGIN (write your solution here)
        return this.size;
        // END
    }

    //true if array is empty
    @Override
    public boolean isEmpty() {
        // BEGIN (write your solution here)
        if (this.size == 0)
            return true;
        else
            return false;
        // END
    }

    @Override
    public boolean contains(final Object o) {
        // BEGIN (write your solution here)
        for ( int i = 0; i<size; i++) {
            if (m[i] == o) return true;
        }
        return false;
        // END
    }

    @Override
    public Iterator<T> iterator() {
        // BEGIN (write your solution here)
    return new ElementsIterator();
        // END
    }

    @Override
    public Object[] toArray() {
        // BEGIN (write your solution here)
        final T[] mNew = (T[]) new Object [this.size()];
        System.arraycopy(m, 0, mNew, 0, this.size());
        return mNew;
        // END
    }

    @Override
    /*This method may prove to be too difficult.
    he test is not covered.*/
    public <T1> T1[] toArray(T1[] a) {
        // BEGIN (write your solution here)
        return (T1[]) this.toArray();   //как тут правильно сделать вызов toArray?
        // END
    }

    @Override
    public boolean add(final T t) {
        // BEGIN (write your solution here)
        if (m.length == size) {
            System.out.println(size);
            //то сохраняем старый и создаем новый массив с size*2
            final T[] oldm = m;
            m = (T[]) new Object [this.size() * 2];
            System.arraycopy(oldm, 0, m, 0, this.size());
        }
        m[size++] = t; //добавляем элемент в слудующую ячейку. записываем в следующий номер массива элемент t, который не входил в массив

        return true;
        // END
    }

    @Override
    public boolean remove(final Object o) {
        // BEGIN (write your solution here)
        for (int i = 0; i < size; i++) {    //если нашли элемент для удаления
            if (m[i].equals(o)) {
                if (i != this.size()-1)
                    //чтобы работало при удалении последнего
                    System.arraycopy(m, i + 1, m, i, this.size() - i - 1);
                size--;
                return true;
            }
        }
        return false;
        // END
    }

    @Override
    public boolean containsAll(final Collection<?> c) {
        // BEGIN (write your solution here)
        for ( Object m : c) {
            if (!contains(m)) return false;
        }
        return true;
        // END
    }

    @Override
    public boolean addAll(final Collection<? extends T> c) {
        // BEGIN (write your solution here)
        //делаю цикл перебора добавляемых элементов
        for (final T item : c) {
            add(item);
        }
        return true;
        // END
    }

    @Override
    public boolean removeAll(final Collection<?> c) {
        // BEGIN (write your solution here)
        for (final Object item : c) {
            remove(item);
        }
        return true;
        // END
    }

    /*@Override
    public boolean retainAll(final Collection<?> c) {
        // BEGIN (write your solution here)
        for (final Object item : this) {
            if (!c.contains(item)) this.remove(item);
            //System.out.println(this.size());
        }
        return true;
        // END
    }*/

    @Override
    public boolean retainAll(Collection<?> c) {
        int x = 0;
        T[] tempNew = (T[]) new Object[this.size];
        for (final Object item : this) {
            if (c.contains(item)) {
                tempNew[x] = (T) item;
                x++;
            }
        }
        m = (T[]) new Object[x];
        for (int i = 0; i < x; i++) {
            m[i] = tempNew[i];
        }
        this.size =x;
        return true;
    }

    @Override
    public void clear() {
        // BEGIN (write your solution here)
//        for (int i = 0; i < this.size; i++) {
//            m[i] = null; }
       m = (T[])new Object[1];
       size = 0;
        // END
    }

    public void printSize () {
        System.out.println(this.size());
        //return 1;
    }

    private class ElementsIterator implements Iterator<T> {
        // BEGIN (write your solution here)

        private int index = 0;

        @Override
        public boolean hasNext() {

            return ArrayCollection.this.size() > index;
        }

        @Override
        public T next() {
            final int currentIndex = index;
            index += 1;
            return ArrayCollection.this.m[currentIndex];
        }
        // END
    }


}